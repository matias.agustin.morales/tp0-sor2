
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>


int init_module(void);
void cleanup_module(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t read_inverse(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);
#define SUCCESS 0
#define DEVICE_NAME "chardev" 
#define BUF_LEN 80              // Largo maximo del mensaje


static int Major;               // Numero Major
static int Device_Open = 0;                  
static char msg[BUF_LEN];       // Mensaje
static char *msg_Ptr;           // Puntero del mensaje
static struct file_operations fops = {
    .read = device_read,        // Cambiar por read_inverse para leer al revez
    .write = device_write,
    .open = device_open,
    .release = device_release
    };


int init_module(void)
{
    Major = register_chrdev(0, DEVICE_NAME, &fops);
    if (Major < 0)
    {
        printk(KERN_ALERT "EL registro del dispositivo char fallo con %d\n", Major);
        return Major;
    }
    printk(KERN_INFO "Fue asignado el numero siguiente Major %d\n", Major);
    printk(KERN_INFO "El driver fue registrado.\n");
    
    return SUCCESS;
}

void cleanup_module(void)
{
    unregister_chrdev(Major, DEVICE_NAME);
    printk(KERN_INFO "EL driver fue removido.\n");
    printk(KERN_INFO "--------------------------------------------\n");
}

static int device_open(struct inode *inode, struct file *file)
{
    if (Device_Open)
        return -EBUSY;
    Device_Open++;
    msg_Ptr = msg;
    try_module_get(THIS_MODULE);
    printk(KERN_INFO "EL driver fue abierto.\n");
    return SUCCESS;
}

static int device_release(struct inode *inode, struct file *file)
{
    Device_Open--;
    module_put(THIS_MODULE);
    printk(KERN_INFO "EL driver fue cerrado.\n");
    return 0;
}

static ssize_t device_read(struct file *filp,char *buffer,size_t length,loff_t *offset)
{
    int bytes_read = 0;

    if (*msg_Ptr == 0)
        return 0;

    while (length && *msg_Ptr) {
        put_user(*(msg_Ptr++), buffer++);
        length--;
        bytes_read++;
    }
    
    printk(KERN_INFO "EL driver fue leido.\n");
    printk(KERN_INFO "Mensaje leido del espacio de usuario: %s", msg);
    
    return bytes_read;
}
   
    
static ssize_t read_inverse(struct file *filp,char *buffer,size_t length,loff_t *offset)
{
    int bytes_read = 0;
    int i = strlen(msg)-1; // Guardo el largo del mensaje en i

    if (*msg_Ptr == 0)
        return 0;

    while (length &&  *msg_Ptr && i>=0) {
        put_user(msg[i], buffer++);
        length--;
        bytes_read++;
        i--;
        *(msg_Ptr++);
    }
    
    printk(KERN_INFO "EL driver fue leido a la inversa.");
    
    return bytes_read;
}

static ssize_t device_write(struct file *file, const char *buffer, size_t length, loff_t *offset)
{
    memset(msg_Ptr,0,80);   // Limpio el msg_Ptr antes de volver a escribir

    ssize_t result = copy_from_user(msg_Ptr, buffer, length);
    if (result < 0)
    {
        printk(KERN_ERR "Error al copiar el búfer de entrada del usuario\n");
        return result;
    }

    printk(KERN_INFO "EL driver fue escrito.\n");
    printk(KERN_INFO "Mensaje recibido del espacio de usuario: %s", msg_Ptr);
    
    return length;
}


MODULE_LICENSE("GPL");