# Tp0-Sor2

## Comandos

```
Para correr el driver:
-make clean
-make

Para montar el driver:
-sudo insmod charDev.ko

Para desmontar el driver:
-sudo rmmod charDev.ko

Script para de montar el modulo y desmontarlo mas rapido:
-./addModulo.sh
-./removeModulo.sh

Para crear directorio:
-sudo mknod /dev/chardev c 240 0

Para enviar texto:
-sudo sh -c 'echo "Hola, mundooooo!" > /dev/chardev'

Para dar permisos:
-sudo chmod 777 /dev/chardev

Para leer texto:
-cat /dev/chardev

Para ver lo que pasa (print en el kernel):
-dmesg

```
## Para instalar los encabezados del kernel en Linux, sigue estos pasos:

```
Actualizar paquetes:
-sudo apt-get update 

Instalar:
-sudo apt-get install linux-headers-$(uname -r) 

Ver si se instalo correctamente:
-dpkg -l | grep linux-headers

```